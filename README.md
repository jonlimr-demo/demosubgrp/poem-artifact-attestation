# Poem Artifact Attestation

### **Note: Do not edit this project directly, but rather clone it and configure it within your own GitLab instance.**

This project uses GitLab [artifact attestation](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#artifact-attestation) in order to detect if a file has been tampered with. Automation of the generation of attestation metadata is enabled by adding the variable `RUNNER_GENERATE_ARTIFACTS_METADATA: true` a job which generates an artifact.

## Detecting tampering in this project

In this example we are checking to see if `poem.txt` has been altered. In order to do this, I have created a [SHA-256 hash](https://www.movable-type.co.uk/scripts/sha256.html#:~:text=A%20cryptographic%20hash%20(sometimes%20called,byte)%20signature%20for%20a%20text.) of poem.txt in it's orignial state:

```bash
$ openssl sha256 poem.txt | awk '{print $2}'
e78f57626b6be5a363d0fe9d193d924b1a04dabe0af5da9c01fa55027463f7bd
```

then I set an enivironment variable in the .gitlab-ci, `EXPECTED_SHA: "e78f57626b6be5a363d0fe9d193d924b1a04dabe0af5da9c01fa55027463f7bd"` to make sure that it's whats always being compared. Then we simply parse the generated **sha256 value** from the generated attestation metadata and compare it with the `EXPECTED_SHA`. If the values are consistent then the file is sound, if they differ, then it has been tampered with. See the [.gitlab-ci.yml](.gitlab-ci.yml) to see the job definitions:

- **gather-metadata** builds the attestation file
- **detect-tampering** verifies the hash of poem.txt with the suggested hash

The MR [Hack the poem](https://gitlab.com/gitlab-de/tutorials/security-and-governance/poem-artifact-attestation/-/merge_requests/1) shows the results of a file that has been tampered with.

**Note:** In order to prevent developers from  changing the `EXPECTED_SHA` value, it is recommended that you implement separation of duties using the following features:

- [CodeOwners](https://docs.gitlab.com/ee/user/project/codeowners/)
- [Compliance Pipelines](https://docs.gitlab.com/ee/user/group/compliance_frameworks.html#compliance-pipelines)

**Note:** The artifacts-metadata generated in a job contains additional fields to help you determine the who/what/when/how of the tampering. See the [artifact attestation documentation](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#artifact-attestation) for more information.

---

Created and maintained by [Fern](https://gitlab.com/fjdiaz)🌿

- [LinkedIn](https://www.linkedin.com/in/awkwardferny/)
- [Twitter](https://twitter.com/awkwardferny)